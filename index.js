"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var title_service_1 = require("./peek-util/title.service");
exports.TitleService = title_service_1.TitleService;
var footer_service_1 = require("./peek-util/footer.service");
exports.FooterService = footer_service_1.FooterService;
var nav_back_service_1 = require("./peek-util/nav-back.service");
exports.NavBackService = nav_back_service_1.NavBackService;
var platform_1 = require("./peek-util/platform");
exports.DeviceSizeE = platform_1.DeviceSizeE;
//# sourceMappingURL=index.js.map