"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DeviceSizeE;
(function (DeviceSizeE) {
    DeviceSizeE[DeviceSizeE["dpi300"] = 0] = "dpi300";
    DeviceSizeE[DeviceSizeE["dpi400"] = 1] = "dpi400";
    DeviceSizeE[DeviceSizeE["default"] = 2] = "default";
})(DeviceSizeE = exports.DeviceSizeE || (exports.DeviceSizeE = {}));
//# sourceMappingURL=platform.js.map