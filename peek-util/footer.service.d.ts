import { Subject } from 'rxjs';
export interface ConfigLink {
    'plugin': string;
    'route': string;
    'text': string;
}
export declare class FooterService {
    statusText: Subject<string>;
    statusTextSnapshot: string;
    configLinks: Subject<ConfigLink[]>;
    configLinksSnapshot: ConfigLink[];
    constructor();
    setLinks(links: ConfigLink[]): void;
    setStatusText(title: string): void;
}
