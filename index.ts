export {TitleService, TitleBarLink} from "./peek-util/title.service";
export {FooterService, ConfigLink} from "./peek-util/footer.service";
export {NavBackService} from "./peek-util/nav-back.service";
export {PlatformI, DeviceSizeE} from "./peek-util/platform";
export {Sound} from "./peek-util/sound";
